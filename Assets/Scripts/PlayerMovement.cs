using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;


public class PlayerMovement : MonoBehaviour, InputActions.IPlayerActions
{
    private Camera _mainCamera;
    private Rigidbody _rigidbody;
    private InputActions _inputAction;
    private Vector2 _moveInput;
    public float speed = 5f;

    public static PlayerMovement Instance;

    [SerializeField] private float jumpforce;
    [SerializeField] private float coyoteTime = 0.3f;
    [SerializeField] private float forwardLerpSpeed = 0.3f;
    [SerializeField] private float movementTightness = 0.3f;
    [SerializeField] private float inputSmoothing = 0.3f;
    [SerializeField] private LayerMask playerLayer;
    [SerializeField] private Transform feetPosition;
    [SerializeField] private float gravityStrength = 1f;
    [SerializeField] private float gravityStrengthPressedJump = 0.5f;
    [SerializeField] private float maxFallSpeed = 30f;
    [SerializeField] private ParticleSystem boostParticle;

    private float leftGroundTime = -100f;
    private RaycastHit[] groundCheckArray = new RaycastHit[10];

    public readonly UnityEvent OnHitGround = new();
    public readonly UnityEvent OnDidJump = new();

    private void Awake()
    {
        Instance = this;
    }

    public void ResetVelocity()
    {
        _rigidbody.velocity = Vector3.zero;
        desiredVelocity = Vector3.zero;
    }

    private void Start()
    {
        _mainCamera = Camera.main;
        _moveInput = new Vector2();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        SmoothInputVector();
    }

    void LateUpdate()
    {
        FaceInMoveDirection();
    }
    
    private void FixedUpdate()
    {
        ApplyMoveRelativeToCamera();
        CheckIsGrounded();
        CustomGravity();
        LimitFallSpeed();
    }

    private void OnEnable()
    {
        if (_inputAction != null)
            return;

        _inputAction = new InputActions();
        _inputAction.Player.SetCallbacks(this);
        _inputAction.Player.Enable();
    }

    private bool justJumped = false;

    private void CustomGravity()
    {
        if (justJumped && Keyboard.current.spaceKey.isPressed == false)
        {
            justJumped = false;
        }

        var boostParticleEmission = boostParticle.emission;
        boostParticleEmission.rateOverTime = justJumped ? 3000f : 0f;
        
        _rigidbody.AddForce(Vector3.down * (justJumped? gravityStrengthPressedJump : gravityStrength) , ForceMode.Acceleration);   
    }

    public void OnDisable()
    {
        _inputAction.Player.Disable();
    }

    public void OnLook(InputAction.CallbackContext context)
    {
    }

    private Vector2 desiredMoveInput;
    public void OnMove(InputAction.CallbackContext context)
    {
        Vector2 moveVector = context.ReadValue<Vector2>();
        desiredMoveInput = moveVector;
    }

    private bool jumped = false;
    private float timeJumped;
    public void OnJump(InputAction.CallbackContext context)
    {
        if (context.ReadValueAsButton() == false)
        {
            return;
        }
        if (!IsGrounded && leftGroundTime + coyoteTime < Time.time) return;
        if (jumped)
        {
            return;
        }
        
        OnDidJump.Invoke();
        _rigidbody.AddForce(Vector3.up * jumpforce, ForceMode.VelocityChange);
        justJumped = true;
        timeJumped = Time.time;
        jumped = true;
    }

    

    private Vector3 desiredVelocity = new();
    private Vector3 movementVec = new();

    private void ApplyMoveRelativeToCamera()
    {
        var cameraTransform = _mainCamera.transform;
        var cameraForward = cameraTransform.forward;
        var cameraRight = cameraTransform.right;

        cameraForward = new Vector3(cameraForward.x, 0, cameraForward.z);
        cameraRight = new Vector3(cameraRight.x, 0, cameraRight.z);

        cameraForward = cameraForward.normalized;
        cameraRight = cameraRight.normalized;

        var direction = cameraForward * _moveInput.y + cameraRight * _moveInput.x;
        if (direction.magnitude > 1f)
        {
            direction = direction.normalized;
        }
        desiredVelocity = direction * speed;
        movementVec = Vector3.Lerp(movementVec, desiredVelocity, movementTightness*Time.fixedTime*50);
        _rigidbody.velocity = new Vector3(movementVec.x, _rigidbody.velocity.y, movementVec.z);
    }


    [SerializeField]
    private bool IsGrounded = false;


    private void CheckIsGrounded()
    {
        if (timeJumped + 0.1f > Time.time)
        {
            return;
        }
        var foundNumber = Physics.SphereCastNonAlloc(feetPosition.position, 0.2f, -Vector3.up, groundCheckArray, 0.05f, playerLayer);
        
        Debug.DrawLine(feetPosition.position, feetPosition.position-Vector3.up*0.2f, Color.red, 0.1f);
        var groundedThisFrame = foundNumber > 0;
        if (IsGrounded && groundedThisFrame == false)
        {
            leftGroundTime = Time.time;
        }

        if (IsGrounded == false && groundedThisFrame == true)
        {
            justJumped = false;
            OnHitGround.Invoke();
        }
        
        IsGrounded = groundedThisFrame;
        if (IsGrounded)
        {
            leftGroundTime = Time.time;
            if (timeJumped+0.05f < Time.time)
            {
                jumped = false;
            }
        }
    }

    private void LimitFallSpeed()
    {
        var vel = _rigidbody.velocity;
        _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, Mathf.Clamp(_rigidbody.velocity.y, -maxFallSpeed, 200),
            _rigidbody.velocity.z);
    }

    private void SmoothInputVector()
    {
        _moveInput = Vector2.Lerp(_moveInput, desiredMoveInput, inputSmoothing * Time.deltaTime * 60f);
    }

    private void FaceInMoveDirection()
    {
        var rigidbodyVelocity = _rigidbody.velocity;
        if (new Vector3(rigidbodyVelocity.x, 0f, rigidbodyVelocity.z).magnitude < 0.02f) return;
        transform.rotation = Quaternion.Slerp(
            transform.rotation,
            Quaternion.LookRotation(new Vector3(rigidbodyVelocity.x, 0, rigidbodyVelocity.z)),
            Time.deltaTime * 50f * forwardLerpSpeed
        );
    }
}