using UnityEngine;

namespace ROTP
{
    public class UltimateSmoother : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private float smoothness = 1f;
        [SerializeField] private float smoothnessRotation = 1f;

        private void Update()
        {
            transform.position = Vector3.Lerp(transform.position, target.position, 0.5f * Time.deltaTime * 60f * smoothness);
            transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, 0.5f * Time.deltaTime * 60f * smoothnessRotation);
        }
    }
}