using System;
using UnityEngine;

namespace DefaultNamespace
{
    public class SmoothFollow : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private float smoothness = 1f;
        [SerializeField] private float smoothnessForwardVec = 1f;

        private void Update()
        {
            transform.position = Vector3.Lerp(transform.position, target.position, 0.5f * Time.deltaTime * 60f * smoothness);
            //transform.forward = Vector3.Lerp(transform.forward, new Vector3(target.forward.x, 0f, target.forward.z), 0.5f * Time.deltaTime * 60f * smoothnessForwardVec);
        }
    }
}