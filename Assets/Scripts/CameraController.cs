using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

namespace ROTP
{
    [RequireComponent(typeof(CinemachineFreeLook))]
    public class CameraController : MonoBehaviour
    {
        private CinemachineFreeLook cinemachineFreeLook;
        private Vector2 inputVec;
        private Vector2 desiredInputVec;
        

        [SerializeField] private float smoothness = 1f;
        [SerializeField] private float overallSens = 1f;
        [SerializeField] private float xSens = 1f;
        [SerializeField] private float ySens = 1f;

        private void Awake()
        {
            cinemachineFreeLook = GetComponent<CinemachineFreeLook>();
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void Update()
        {
            var deltaTime = Mathf.Clamp(Time.deltaTime, 0f,0.02f);
            inputVec = Vector2.Lerp(inputVec, desiredInputVec, 0.4f * deltaTime * 50f * smoothness);
            
            cinemachineFreeLook.m_XAxis.Value += inputVec.x * deltaTime * xSens * overallSens;
            cinemachineFreeLook.m_YAxis.Value += inputVec.y * deltaTime * ySens * overallSens;
        }

        private void OnLook(InputValue value)
        {
            desiredInputVec = value.Get<Vector2>();
        }
    }
}