using System;
using Cinemachine;
using UnityEngine;

namespace ROTP
{
    [RequireComponent(typeof(CinemachineBrain))]
    public class CinemachineBrainUpdater : MonoBehaviour
    {
        private CinemachineBrain cinemachineBrain;

        private void Awake()
        {
            cinemachineBrain = GetComponent<CinemachineBrain>();
        }
        
        private void Update()
        {
            cinemachineBrain.ManualUpdate();
        }
    }
}