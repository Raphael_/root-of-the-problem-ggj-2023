using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace ROTP.UI
{
    [RequireComponent(typeof(Image))]
    public class UIWipe : MonoBehaviour
    {
        private Image image;

        private void Awake()
        {
            image = GetComponent<Image>();
        }

        private void Start()
        {
            RespawnManager.instance.OnRespawn.AddListener(() =>
            {
                image.DOFillAmount(1f, 0.4f).OnComplete(() => { image.DOFillAmount(0f, 0.4f).SetDelay(0.4f); }
                );
            });
        }
    }
}