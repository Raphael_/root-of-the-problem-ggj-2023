using UnityEngine;

namespace ROTP
{
    public class PlayerSquash : MonoBehaviour
    {
        [SerializeField] private float smoothness = 1f;
        [SerializeField] private float durationModifier = 1f;
        [SerializeField] private float intensity = 1f;
        
        private Vector3 desiredScale;
        
        private void Start()
        {
            PlayerMovement.Instance.OnHitGround.AddListener(() =>
            {
                desiredScale = new Vector3(1.23f, .8f, 1.23f) * intensity;
            });
            
            PlayerMovement.Instance.OnDidJump.AddListener(() =>
            {
                desiredScale = new Vector3(.8f,  1.2f, .8f) * intensity;
            });
        }

        private void Update()
        {
            desiredScale = Vector3.Lerp(desiredScale, Vector3.one, 0.4f * Time.deltaTime * 60f * durationModifier);
            transform.localScale = Vector3.Lerp(transform.localScale, desiredScale, 0.4f * Time.deltaTime * 60f * smoothness);
        }
    }
}