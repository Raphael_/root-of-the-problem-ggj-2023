using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class RespawnManager : MonoBehaviour
{
    private Transform player;
    [SerializeField] private float dieYCoordinate = 0f;

    public readonly UnityEvent OnRespawn = new();

    public static RespawnManager instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        player = PlayerMovement.Instance.transform;
    }

    private void Update()
    {
        if (player.transform.position.y < dieYCoordinate)
        {
            if (respawning == false)
            {
                StartCoroutine(Respawn());
            }
        }
    }

    private bool respawning = false;

    public IEnumerator Respawn()
    {
        respawning = true;
        OnRespawn.Invoke();
        yield return new WaitForSeconds(0.4f);
        
        PlayerMovement.Instance.transform.position = transform.position;
        PlayerMovement.Instance.ResetVelocity();
        respawning = false;
    }
}